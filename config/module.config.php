<?php

namespace RocketUsers;

use RocketUsers\Controller\CliController;
use RocketUsers\Factory\AbstractDoctrineMapperFactory;
use RocketUsers\Factory\Controller\CliControllerFactory;
use RocketUsers\Factory\Listener\AccessListenerFactory;
use RocketUsers\Factory\Listener\UserEntityListenerFactory;
use RocketUsers\Factory\Mapper\UserMapperFactory;
use RocketUsers\Factory\Service\AccessServiceFactory;
use RocketUsers\Factory\Strategy\RedirectStrategyFactory;
use RocketUsers\Listener\AccessListener;
use RocketUsers\Listener\UserEntityListener;
use RocketUsers\Mapper\GroupPermissionMapper;
use RocketUsers\Mapper\RoleMapper;
use RocketUsers\Mapper\UserAuthenticationLogMapper;
use RocketUsers\Factory\Controller\Plugin\AuthenticationPluginFactory;
use RocketUsers\Mapper\UserMapper;
use RocketUsers\Mapper\UserPermissionMapper;
use RocketUsers\Mapper\UserResetTokenMapper;
use RocketUsers\Service\AccessService;
use RocketUsers\Service\AuthenticationService;
use RocketUsers\Factory\Service\AuthenticationServiceFactory;
use RocketUsers\Strategy\RedirectStrategy;
use RocketUsers\View\Helper\AuthHelperFactory;

return [

    /**
     * This module's default configuration block.  There are more values that should be customized inside of
     * rocket.user.local.php.dist.  Check that file out for more.
     */
    'rocket' => [
        'user' => [
            'providers' => [
                'role' => RoleMapper::class,
                'rules' => [
                    'group' => GroupPermissionMapper::class,
                    'user' => UserPermissionMapper::class,
                ],
                'reset' => UserResetTokenMapper::class,
            ],
        ],
    ],

    'doctrine' => [
        'eventmanager' => [
            'orm_default' => [
                'subscribers' => [
                    UserEntityListener::class,
                ],
            ],
        ],

        'driver' => [
            'rocket_entities' => [
                'class' => 'Doctrine\ORM\Mapping\Driver\AnnotationDriver',
                'paths' => realpath(__DIR__ . '/../src/RocketUsers/Entity'),
            ],

            'orm_default' => [
                'drivers' => [
                    'RocketUsers\Entity' => 'rocket_entities',
                ],
            ],
        ],
    ],

    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers' => [
        'factories' => [
            RocketUsers\View\Helper\AuthHelper::class => AuthHelperFactory::class,
        ],
        'aliases' => [
            'auth' => RocketUsers\View\Helper\AuthHelper::class,
        ]
    ],

    'controller_plugins' => [
        'factories' => [
            'auth' => AuthenticationPluginFactory::class,
        ],
    ],

    'service_manager' => [

        'invokables' => [
            UserAuthenticationLogMapper::class => UserAuthenticationLogMapper::class,
        ],

        'factories' => [
            AuthenticationService::class => AuthenticationServiceFactory::class,
            AccessService::class => AccessServiceFactory::class,
            AccessListener::class => AccessListenerFactory::class,
            UserEntityListener::class => UserEntityListenerFactory::class,
            UserMapper::class => UserMapperFactory::class,
            RedirectStrategy::class => RedirectStrategyFactory::class,
        ],

        'abstract_factories' => [
            AbstractDoctrineMapperFactory::class,
        ],
    ],

    'controllers' => [
        'factories' => [
            CliController::class => CliControllerFactory::class,
        ],
    ],

    'console' => [
        'router' => [
            'routes' => [

                'rocket-role-grant' => [
                    'options' => [
                        'route' => 'grant resource-role <roleName> <resourceClass> <resourceId> <verb>',
                        'defaults' => [
                            'controller' => CliController::class,
                            'action' => 'grant-resource-role',
                        ],
                    ],
                ],

                'rocket-user-grant' => [
                    'options' => [
                        'route' => 'grant resource-user <userEmail> <resourceClass> <resourceId> <verb>',
                        'defaults' => [
                            'controller' => CliController::class,
                            'action' => 'grant-resource-user',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
