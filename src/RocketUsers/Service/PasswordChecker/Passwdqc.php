<?php

namespace RocketUsers\Service\PasswordChecker;

use RocketUsers\Provider\PasswordCheckerInterface;

class Passwdqc implements PasswordCheckerInterface
{

    public function isStrongPassword(string $clearPassword): bool
    {
        $implementation = new \ParagonIE\Passwdqc\Passwdqc();

        return $implementation->check($clearPassword);
    }
}