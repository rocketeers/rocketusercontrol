<?php

namespace RocketUsers\Exception;

class GuardExpectedException extends \Exception
{
    public function __construct($controllerName)
    {
        parent::__construct("No rules are configured for guard $controllerName.");
    }

}