<?php
/**
 * Created by PhpStorm.
 * User: danielchaves
 * Date: 08/05/2018
 * Time: 20:35
 */

namespace RocketUsers\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceManager;
use RocketUsers\Service\AuthenticationService;

class AuthHelper extends AbstractHelper
{

    public $services;

    public function __construct(ServiceManager $services)
    {
        $this->services = $services;
    }

    public function __invoke()
    {
        // TODO: Implement __invoke() method.
        $user = $this->services->get(AuthenticationService::class);

        return $user;
    }

}