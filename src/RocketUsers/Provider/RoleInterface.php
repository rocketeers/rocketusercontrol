<?php

namespace RocketUsers\Provider;

/**
 * Interface RoleInterface
 *
 * A user role, with a parent.
 *
 * @package RocketUsers\Provider
 */
interface RoleInterface
{
    public function getId() : int;

    public function getName() : string;

    public function getParent();
}