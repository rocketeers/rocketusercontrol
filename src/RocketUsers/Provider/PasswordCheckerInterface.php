<?php

namespace RocketUsers\Provider;

interface PasswordCheckerInterface
{
    public function isStrongPassword(string $clearPassword): bool;
}