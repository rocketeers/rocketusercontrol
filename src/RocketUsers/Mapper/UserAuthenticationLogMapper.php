<?php

namespace RocketUsers\Mapper;


/**
 * Class UserAuthenticationLogMapper
 *
 * Log when users authenticate
 *
 * @package RocketUsers\Mapper
 */
class UserAuthenticationLogMapper extends AbstractDoctrineMapper
{
    protected $entityName = 'RocketUsers\Entity\UserAuthenticationLog';

}