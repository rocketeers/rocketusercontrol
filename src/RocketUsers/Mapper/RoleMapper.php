<?php

namespace RocketUsers\Mapper;

use RocketUsers\Provider\RoleInterface;
use RocketUsers\Provider\RoleProviderInterface;
use RocketUsers\Provider\UserInterface;

/**
 * Class RoleMapper
 *
 * Get and put roles out of the database
 *
 * @package RocketUsers\Mapper
 */
class RoleMapper extends AbstractDoctrineMapper implements RoleProviderInterface
{
    protected $entityName = 'RocketUsers\Entity\Role';

    public function getAllRoles() : array
    {
        $query = $this->getRepository()->createQueryBuilder('r')
            ->select('r')
                ->getQuery();

        return $query->getResult();
    }


    /**
     * Fetch a role with a particular name
     *
     * @param $name
     *
     * @return mixed
     */
    public function getRoleWithName($name)
    {
        return $this->getRepository()->findOneBy(['name' => $name]);
    }
}