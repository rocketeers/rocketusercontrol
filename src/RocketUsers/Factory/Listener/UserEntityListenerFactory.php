<?php

namespace RocketUsers\Factory\Listener;

use RocketUsers\Listener\UserEntityListener;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class UserEntityListenerFactory implements FactoryInterface
{

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        if (!isset($config['rocket']['user']['doctrine']['entity'])) {
            throw new \Exception("RocketUsers > You must specify the user Entity that RocketUsers will use!");
        }

        return new UserEntityListener($config['rocket']['user']['doctrine']['entity']);
    }
}