<?php

namespace RocketUsers\Factory\Strategy;

use RocketUsers\Strategy\RedirectStrategy;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class RedirectStrategyFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        if (!isset($config['rocket']['user']['deny_strategy']['options'])) {
            throw new \Exception("RocketUsers > Please check your config. You specified the module-provided redirect strategy, but didn't include the provided configuration.");
        }
        $options = $config['rocket']['user']['deny_strategy']['options'];

        return new RedirectStrategy($options['controller'], $options['action']);

    }
}